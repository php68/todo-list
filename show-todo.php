<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <style media="screen">
      body{
        padding: 50px!important;
      }
    </style>
    <meta charset="utf-8">
    <title>todo list</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
  </head>
  <body>

    <?php
    require "conect-database.php";

      if (isset($_REQUEST['delet'])) {
        $idtodo =  intval($_REQUEST['delet']);
        $sql = "DELETE FROM TODO WHERE id=$idtodo";
        $conn->query($sql);
        echo "<script>alert('Deleted !')</script>";
        echo "<script>window.location.href='show-todo.php'</script>";
        }

     ?>



     <hr>
    <a href="add.php">
      <div class="ui vertical animated button" tabindex="0" style="margin-bottom:50px; ">
        <div class="hidden content">Add</div>
        <div class="visible content">
          <i class="plus icon"></i>
        </div>
      </div>
    </a>


    <?php

       require "conect-database.php";
       $sql = "SELECT id,title,do,reg_date FROM TODO";
       $results = $conn->query($sql);
       foreach ($results as $result) {?>

    <div class="ui cards">
      <div class="card">
        <div class="content">
          <div class="header">
            <?php echo $result['title']; ?>
          </div>
          <div class="meta">
            <?php echo $result['reg_date']; ?>
          </div>
          <div class="description">
            <?php echo $result['do']; ?>
          </div>
        </div>
        <div class="extra content">
          <div class="ui two buttons">
            <a href="edit.php?edit=<?php echo $result['id'] ?>" class="ui basic green button">Edit</a>
            <a href="show-todo.php?delet=<?php echo $result['id']; ?>" name="delet"  class="ui basic red button">Delete</a>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>

  </body>
</html>
